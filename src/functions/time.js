export function timestampToDate(seconds) {
    var date = new Date(null);
    date.setSeconds(seconds / 1000);
    return date.toLocaleTimeString() + ', ' + date.toLocaleDateString();
}

export function timestampToHour(seconds) {
    var date = new Date(null);
    date.setSeconds(seconds);
    return date.toISOString().substr(11, 8);
}
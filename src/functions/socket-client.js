
export class SocketClient {
    ws = null;
    username = null;
    connected = false;
    serverAddress = "wss://imr3-react.herokuapp.com";

    messageListener = (message) => { console.log('message received') };

    connect(username) {
        this.username = username;
        this.ws = new WebSocket(this.serverAddress);
        this.ws.onopen = () => this.onOpen();
        this.ws.onmessage = (evt) => this.onMessage(evt);
        this.ws.onclose = () => this.onClose();
    }

    onOpen() {
        console.log('ws client connected');
        this.connected = true;
    }

    onMessage(evt) {
        const messages = JSON.parse(evt.data);
        this.messageListener(messages);
    }

    setMessageListener(callback) {
        this.messageListener = callback;
    }

    sendMessage(message) {
        this.ws.send(JSON.stringify({
            message: message,
            name: this.username
        }));
    }

    onClose() {
        console.log('ws client disconnected');
        this.username = null;
        this.connected = false;
    }
}
/*
let client = new SocketClient();
client.setMessageListener((message) => {
    console.log('message', message);
});
client.connect('billy');
*/
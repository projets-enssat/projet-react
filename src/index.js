import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import { Page } from "./components/page";
import { map } from 'leaflet';

class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loading: true, data: null };
    }

    componentDidMount() {
        fetch('https://imr3-react.herokuapp.com/backend')
            .then(resp => resp.json())
            .then(json => this.setState({
                loading: false, data: {
                    film: json.Film,
                    chapters: json.Chapters,
                    keywords: json.Keywords,
                    waypoints: json.Waypoints
                }
            }));
    }

    render() {
        const { loading, data } = this.state;
        console.log(data);
        return (
            <Fragment>
                {loading ? <div>Loading...</div> : <Page data={data} />}
            </Fragment>
        );
    }
}

ReactDOM.render(<Content />, document.getElementById("root"));

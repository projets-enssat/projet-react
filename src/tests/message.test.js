import React from 'react';
import { render } from '@testing-library/react';
import { Message } from '../components/message';

test("renders without crashing", () => {
    const div = document.createElement("div");
    render(<Message key="0" when="55555" name="Dupont" message="Hello world" />, div);
});
import React from 'react';
import { render } from '@testing-library/react';
import { Carte } from '../components/carte';


test("renders without crashing", () => {
    const div = document.createElement("div");
    const waypoint = { lat: 48.7333, lng: -3.4667, label: 'Lannion' };
    render(<Carte waypoints={[waypoint]} />, div);
});
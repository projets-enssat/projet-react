import React from 'react';
import { render } from '@testing-library/react';
import { Keywords } from '../components/keywords';

test("renders without crashing", () => {
    const div = document.createElement("div");
    const keyword = {
        pos: "0", data: [
            {
                "title": "Route 66",
                "url": "https://en.wikipedia.org/wiki/U.S._Route_66"
            },
            {
                "title": "Stefan Kluge",
                "url": "http://www.imdb.com/name/nm1667631/"
            },
            {
                "title": "Mathias Einmann",
                "url": "http://www.imdb.com/name/nm1667578/"
            }
        ]
    };
    render(<Keywords keywords={[keyword]} />, div);
});
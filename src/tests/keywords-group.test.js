import React from 'react';
import { render } from '@testing-library/react';
import { KeywordsGroup } from '../components/keywords-group';

test("renders without crashing", () => {
    const div = document.createElement("div");
    const data = [
        {
            "title": "Route 66",
            "url": "https://en.wikipedia.org/wiki/U.S._Route_66"
        },
        {
            "title": "Stefan Kluge",
            "url": "http://www.imdb.com/name/nm1667631/"
        },
        {
            "title": "Mathias Einmann",
            "url": "http://www.imdb.com/name/nm1667578/"
        }
    ];

    const pos = "0";
    render(<KeywordsGroup key={pos} keywords={data} />, div);
});
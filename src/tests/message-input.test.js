import React from 'react';
import { render } from '@testing-library/react';
import { SocketClient } from '../functions/socket-client';
import { MessageInput } from '../components/message-input';

test("renders without crashing", () => {
    const div = document.createElement("div");
    const client = new SocketClient();
    render(<MessageInput client={client} />, div);
});
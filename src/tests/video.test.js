import React from 'react';
import { render } from '@testing-library/react';
import { Video } from '../components/video';

test("renders without crashing", () => {
    const div = document.createElement("div");
    const video = {
        file_url: "https://ia801406.us.archive.org/32/items/Route_66_-_an_American_badDream/Route_66_-_an_American_badDream_512kb.mp4",
        title: "Route 66, An American (Bad) Dream",
        synopsis_url: "https://wiki.creativecommons.org/wiki/Route_66_-_An_American_(bad)_Dream"
    };
    render(<Video film={video} />, div);
});
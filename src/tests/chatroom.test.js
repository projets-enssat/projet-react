import React from 'react';
import { render } from '@testing-library/react';
import { ChatRoom } from '../components/chatroom';

test("renders without crashing", () => {
    const div = document.createElement("div");
    render(<ChatRoom />, div);
});
import React from 'react';
import { render } from '@testing-library/react';
import { Chapters } from '../components/chapters';

test("renders without crashing", () => {
    const div = document.createElement("div");
    const chapter = { pos: '0', title: 'Intro' };
    render(<Chapters chapters={[chapter]} />, div);
});
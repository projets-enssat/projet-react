import React from 'react';
import { render } from '@testing-library/react';
import { mock_data } from '../mock/data';
import { Page } from '../components/page';

test("renders without crashing", () => {
    const div = document.createElement("div");
    const data = mock_data;
    render(<Page data={data} />, div);
});

import React from 'react';
import { Message } from './message';
import { MessageInput } from './message-input';
import { SocketClient } from '../functions/socket-client';

export class ChatRoom extends React.Component {
    client = new SocketClient();
    messages = [];

    constructor() {
        super();

        this.client.setMessageListener((message) => {
            this.onMessage(message)
        });
    }

    onMessage(messages) {
        for (let k = 0; k < messages.length; k++) {
            this.messages.push(messages[k]);
        }
        // c'est sale mais c'est react
        this.forceUpdate();
    }

    render() {
        return (
            <main class="chatroom" style={{ gridColumn: 2, gridRow: 2, maxHeight: '200px', overflow: 'scroll' }}>
                <MessageInput client={this.client} />
                <div>
                    {this.messages.map(
                        ({ when, name, message }, i) =>
                            <Message key={i} when={when} name={name} message={message} />
                    )}
                </div>
            </main>
        )
    }
}
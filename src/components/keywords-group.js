import React from 'react';

// selected dark colors
const colors = [
    '#2D848A',
    '#36151E',
    '#001242',
    '#040F16',
    '#E77728',
    '#138A36',
    '#285238',
    '#DB5461'
]

function randint(a, b) {
    return Math.floor(
        (b - a) * Math.random() + a
    );
}

function randomColor() {
    return colors[randint(0, colors.length)];
}

export class KeywordsGroup extends React.Component {
    render() {
        let style = {
            padding: '5px',
            borderRadius: '5px',
            backgroundColor: randomColor(),
            display: 'inline-block',
            margin: '2px',
            textDecoration: 'none',
            color: 'white'
        }
        return (
            this.props.keywords.map(
                ({ url, title }, i) =>
                    <a key={i} style={style} href={url} target="blank">
                        {title}
                    </a>
            )
        )
    }
}
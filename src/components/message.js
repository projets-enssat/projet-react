import React from 'react';
import { timestampToDate } from '../functions/time';


export class Message extends React.Component {
    render() {
        const style = {
            display: 'flex',
            backgroundColor: 'red',
            padding: '8px'
        };

        return (
            <div style={style} >
                <span style={{ fontWeight: 'bold' }}>
                    {this.props.name}:
                </span>
                <span style={{ flex: 1, margin: '0 8px' }}>
                    {this.props.message}
                </span>
                <span>
                    {timestampToDate(this.props.when)}
                </span>
            </div>
        );
    }
}
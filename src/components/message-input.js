import React from 'react';

export class MessageInput extends React.Component {
    input = "";

    renderForm(placeholder, action) {
        const form_style = {
            display: 'flex',
            height: '25px',
        };

        const input_style = {
            'flex': 1
        }

        const btn_style = {
            minWidth: '130px'
        }

        return (
            <form style={form_style} onSubmit={(evt) => { this.handleSubmit(evt) }}>
                <input style={input_style} type="text" name="text" placeholder={placeholder} onChange={(evt) => this.handleChange(evt)} />
                <button style={btn_style} type="submit">{action}</button>
            </form>
        )
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        if (this.props.client.connected) {
            // action pour envoyer un messag
            this.props.client.sendMessage(this.state.text);
        } else {
            // set username and connect to ws
            this.props.client.connect(this.state.text);
        }
        event.preventDefault();
    }

    render() {
        const placeholder = (this.props.client.connected)
            ? 'Enter your message' : 'Enter your username';
        const action = (this.props.client.connected) ? 'envoyer' : 'connexion';

        return (
            <main style={{ display: 'flex', flexDirection: 'column', width: '100%', backgroundColor: 'blue' }}>
                {this.renderForm(placeholder, action)}
            </main>
        );
    }
}
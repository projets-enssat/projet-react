import React from 'react';
import "video-react/dist/video-react.css"; // import css
import { Player } from 'video-react';


export class Video extends React.Component {
    constructor(props) {
        super(props);
        this.state = { player: null, currentTime: 0 };
    }


    componentDidMount() {
        // subscribe state change
        this.player.subscribeToStateChange(this.handleStateChange.bind(this));
    }

    handleStateChange(state, prevState) {
        // copy player state to this component's state
        this.setState({
            player: state,
            currentTime: state.currentTime
        });
        //
        this.props.onChange(this.state.currentTime);
    }

    render() {
        return (
            <main style={{ gridColumn: 1, gridRow: '1 / span 2' }}>
                <Player
                    ref={(player) => { this.player = player }}
                    playsInline
                    src={this.props.url}
                />
            </main>
        )
    }
}
import React from "react";
import PropTypes from "prop-types";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";

export class Carte extends React.Component {
  static propTypes = {
    waypoints: PropTypes.array.isRequired
  };

  getWayPoint() {
    const timestamp = this.props.currentTime;

    for (let i = 0; i < this.props.waypoints.length - 1; i++) {
      if (this.props.waypoints[i].timestamp <= timestamp && timestamp < this.props.waypoints[i + 1].timestamp) {
        return this.props.waypoints[i];
      }
    }
    return this.props.waypoints[this.props.waypoints.length - 1];
  }

  render() {
    const { lat, lng, label } = this.getWayPoint(this.props.waypoints);

    const marker = (
      <Marker position={[lat, lng]}>
        <Popup>{label}</Popup>
      </Marker>
    );

    return (
      <Map center={[lat, lng]} zoom={13} style={{ gridColumn: 2, gridRow: 1 }}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution="&copy; Prieur Baptiste | Le Deunff Sylvan"
        />
        {marker}
      </Map>
    );
  }
}

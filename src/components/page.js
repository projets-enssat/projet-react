import React from "react";
import PropTypes from "prop-types";

import { Carte } from './carte';
import { Chapters } from './chapters';
import { ChatRoom } from './chatroom';
import { Keywords } from './keywords';
import { Video } from './video';

export class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = { currentTime: 0 };
  }

  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  onTimeChange(time) {
    this.setState({ currentTime: time })
  }

  render() {
    const { film, chapters, keywords, waypoints } = this.props.data;

    const container = {
      'display': 'grid',
      'gridTemplateColumns': '2fr 1fr',
      'gridTemplateRows': 'minmax(60vh, 60vh) minmax(40vh 40vh) auto'
    }

    return (
      <main style={container}>
        <Video url={film.file_url} onChange={this.onTimeChange.bind(this)} />
        <Carte waypoints={waypoints} currentTime={this.state.currentTime} />
        <ChatRoom />


        <Chapters chapters={chapters} film={film} currentTime={this.state.currentTime} />
        <Keywords keywords={keywords} currentTime={this.state.currentTime} />
      </main >
    )
  }
}

/*
<Carte

*/
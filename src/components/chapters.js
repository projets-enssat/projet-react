import React from 'react';
import { timestampToHour } from '../functions/time';


export class Chapters extends React.Component {

    isActive(chapter) {
        const i = this.props.chapters.indexOf(chapter);
        const nextChapter = i < this.props.chapters.length - 1 ? this.props.chapters[i + 1] : null;

        if (
            this.props.currentTime >= chapter.pos
            && (!nextChapter || this.props.currentTime < nextChapter.pos)) {
            return true;
        } else {
            return false;
        }

    }

    render() {
        return <main class="chapters" style={{ gridColumn: 1, gridRow: 3 }}>
            <ul>
                <h1>{this.props.film.title}</h1>
                <a href={this.props.film.synopsis_url}>Synopsis</a>

                {this.props.chapters.map(
                    (chapter, i) => {
                        return (
                            <li key={i}
                                title={timestampToHour(chapter.pos)}
                                style={{ 'backgroundColor': this.isActive(chapter) ? 'blue' : null }}
                            >
                                {chapter.title}
                            </li>
                        )
                    }
                )}
            </ul>
        </main>
    }
}
import React from 'react';
import { KeywordsGroup } from './keywords-group';

export class Keywords extends React.Component {
    render() {
        return <main style={{ gridColumn: 2, gridRow: 3 }}>
            {(
                this.props.keywords.map(({ pos, data }) => <KeywordsGroup key={pos} keywords={data} />)
            )}
        </main>
    }
}